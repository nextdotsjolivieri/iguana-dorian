package com.nextdots.iguanafix.data.models;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;


import com.nextdots.iguanafix.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class PermissionResult {

    //----------------------------------------------------------------------------------------------
    // Variables
    //----------------------------------------------------------------------------------------------

    private Activity activity;

    private int requestCode;

    private boolean isAllGranted;
    private boolean isAllDenied;
    private boolean isAllNeverShowAgain;
    private boolean hasSomeNeverShowAgain;
    private boolean hasSomeGranted;
    private boolean hasSomeDenied;

    private List<String> permissions;
    private List<String> granted;
    private List<String> denied;
    private List<String> shouldShowRequestRationale;

    //----------------------------------------------------------------------------------------------
    // Constructors
    //----------------------------------------------------------------------------------------------

    private PermissionResult(Activity activity, int requestCode, @NonNull String[] permissions,
                             @NonNull int[] grantResults) {
        this.permissions = new ArrayList<>();
        granted = new ArrayList<>();
        denied = new ArrayList<>();
        shouldShowRequestRationale = new ArrayList<>();
        this.requestCode = requestCode;
        this.activity = activity;
        this.permissions = Arrays.asList(permissions);

        validate(grantResults);
    }

    public static PermissionResult getResult(Activity activity,
                                             int requestCode,
                                             @NonNull String[] permissions,
                                             @NonNull int[] grantResults) {
        return new PermissionResult(activity, requestCode, permissions, grantResults);
    }

    //----------------------------------------------------------------------------------------------
    // Getter & Setters Methods
    //----------------------------------------------------------------------------------------------

    public int getRequestCode() {
        return requestCode;
    }

    public boolean isAllGranted() {
        return isAllGranted;
    }

    public boolean isAllDenied() {
        return isAllDenied;
    }

    public boolean isAllNeverShowAgain() {
        return isAllNeverShowAgain;
    }

    public boolean hasSomeGranted() {
        return hasSomeGranted;
    }

    public boolean hasSomeDenied() {
        return hasSomeDenied;
    }

    public boolean hasSomeNeverShowAgain() {
        return hasSomeNeverShowAgain;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public List<String> getGranted() {
        return granted;
    }

    public List<String> getDenied() {
        return denied;
    }

    public List<String> getPermissionsShouldShowRationale() {
        return shouldShowRequestRationale;
    }

    public List<String> getDeniedTitles() {
        if (denied == null || denied.size() == 0) {
            return null;
        }

        List<String> result = new ArrayList<>();
        for (String permission : denied) {
            result.add(getPermissionName(permission));
        }
        return result;
    }

    public String getDeniedTitlesAsString() {
        String result = "";
        for (int i = 0, j = denied.size(); i < j; i++) {
            String permission = denied.get(i);
            if (i == 0) {
                result = permission;
            } else if (j == 2 && i == j - 1) {
                result = String.format("%s %s", activity.getString(R.string.all_and), permission);
            } else if (i == j - 1) {
                result = String.format(", %s", permission);
            } else {
                result = String.format("%s %s", activity.getString(R.string.all_and), permission);
            }
        }
        return result;
    }

    public String getPermissionName(String permission) {
        switch (permission) {
            case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                return "Write";

            case Manifest.permission.READ_EXTERNAL_STORAGE:
                return "Read";

            case Manifest.permission.CAMERA:
                return "Camera";

            default:
                return "";
        }
    }

    //----------------------------------------------------------------------------------------------
    // Methods
    //----------------------------------------------------------------------------------------------

    private void validate(@NonNull int[] grantResults) {
        for (int i = 0; i < permissions.size(); i++) {
            String permission = permissions.get(i);
            int grantResult = grantResults[i];
            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                hasSomeGranted = true;
                granted.add(permission);
            } else {
                hasSomeDenied = true;
                denied.add(permission);

                if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                    hasSomeNeverShowAgain = true;
                    shouldShowRequestRationale.add(permission);
                }
            }
        }

        if (this.permissions.size() == granted.size()) {
            isAllGranted = true;
        } else if (this.permissions.size() == denied.size()) {
            isAllDenied = true;
        }
        if (this.permissions.size() == shouldShowRequestRationale.size()) {
            isAllNeverShowAgain = true;
        }
    }
}
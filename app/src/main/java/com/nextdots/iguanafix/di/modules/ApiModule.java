package com.nextdots.iguanafix.di.modules;



import com.nextdots.iguanafix.api.services.ContactAPi;
import com.nextdots.iguanafix.utils.Constant;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@Module
public class ApiModule {

    @Provides
    @Singleton
    public Retrofit retrofit(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .baseUrl(Constant.URL_SERVER)
                .build();
    }

    @Provides
    @Singleton
    public ContactAPi cityAPi(Retrofit retrofit) {
        return retrofit.create(ContactAPi.class);
    }

}

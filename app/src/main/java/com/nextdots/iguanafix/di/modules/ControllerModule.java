package com.nextdots.iguanafix.di.modules;


import com.nextdots.iguanafix.api.controllers.ContactController;
import com.nextdots.iguanafix.api.services.ContactAPi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ControllerModule {


    @Provides
    @Singleton
    ContactController contactController(ContactAPi contactAPi) {
        return new ContactController(contactAPi);
    }
}

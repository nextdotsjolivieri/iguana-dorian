package com.nextdots.iguanafix.di.components;


import com.nextdots.iguanafix.di.modules.ApiModule;
import com.nextdots.iguanafix.di.modules.ApplicationModule;
import com.nextdots.iguanafix.di.modules.ControllerModule;
import com.nextdots.iguanafix.ui.modules.detail.DetailContactFragment;
import com.nextdots.iguanafix.ui.modules.main.MainActivity;
import com.nextdots.iguanafix.ui.modules.main.MainFragment;
import com.nextdots.iguanafix.ui.modules.splash.SplashFragment;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                ApiModule.class,
                ControllerModule.class
        }
)

public interface ApplicationComponent {

    void inject(MainActivity mainActivity);

    void inject(MainFragment mainFragment);

    void inject(DetailContactFragment detailContactFragment);

    void inject(SplashFragment splashFragment);
}

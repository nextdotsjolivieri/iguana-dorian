package com.nextdots.iguanafix.ui.modules.splash;


import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class SplashFragmentPresenter {

    private final SplashFragmentView splashFragmentView;

    public SplashFragmentPresenter(SplashFragmentView splasFragmentView) {
        this.splashFragmentView = splasFragmentView;
    }

    public void sleepTime(){
        Observable.timer(3, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {
                        splashFragmentView.gotoMainFragment();
                    }

                    @Override
                    public void onError(Throwable e) {
                        splashFragmentView.gotoMainFragment();
                    }

                    @Override
                    public void onNext(Long aLong) {}
                });
    }
}

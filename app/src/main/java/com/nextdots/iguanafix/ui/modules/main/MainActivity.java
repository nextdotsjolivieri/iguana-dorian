package com.nextdots.iguanafix.ui.modules.main;

import android.content.Context;
import android.os.Bundle;

import com.nextdots.iguanafix.R;
import com.nextdots.iguanafix.ui.base.BaseActivity;
import com.nextdots.iguanafix.ui.modules.splash.SplashFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends BaseActivity {

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        if (savedInstanceState == null){
            pushFragment(SplashFragment.newInstance(), R.id.container, false);
        } else {
            pushFragment(MainFragment.newInstance(), R.id.container, false);
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}

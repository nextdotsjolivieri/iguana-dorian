package com.nextdots.iguanafix.ui.modules.splash;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.nextdots.iguanafix.R;
import com.nextdots.iguanafix.ui.base.BaseFragment;
import com.nextdots.iguanafix.ui.modules.main.MainFragment;

import butterknife.BindView;


public class SplashFragment extends BaseFragment implements SplashFragmentView{
    private static final String ARG_PARAM_FINISHEP = "finishep";

    private SplashFragmentPresenter splashFragmentPresenter;

    @BindView(R.id.ivPinBlue)
    ImageView ivPinBlue;
    @BindView(R.id.ivPinGreen)
    ImageView ivPinGreen;

    private boolean finished_splash = false;

    public SplashFragment() {
        // Required empty public constructor
    }

    public static SplashFragment newInstance() {
       return new SplashFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
        if(savedInstanceState != null){
            finished_splash = savedInstanceState.getBoolean(ARG_PARAM_FINISHEP);
        }
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        ivPinBlue.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.anim_left_pin));
        ivPinGreen.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.anim_right_pin));
        splashFragmentPresenter.sleepTime();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(finished_splash){
            pushFragment(MainFragment.newInstance(), R.id.container, false);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(ARG_PARAM_FINISHEP, finished_splash);
    }

    private void initialize() {
        getApplicationComponent().inject(this);
        splashFragmentPresenter = new SplashFragmentPresenter(this);
    }

    @Override
    public void gotoMainFragment() {
        finished_splash = true;
        pushFragment(MainFragment.newInstance(), R.id.container, false);
    }
}

package com.nextdots.iguanafix.ui.modules.main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nextdots.iguanafix.R;
import com.nextdots.iguanafix.api.controllers.ContactController;
import com.nextdots.iguanafix.data.models.Contact;
import com.nextdots.iguanafix.ui.adapters.ContactAdapter;
import com.nextdots.iguanafix.ui.base.BaseFragment;
import com.nextdots.iguanafix.ui.modules.detail.DetailContactFragment;
import com.nextdots.iguanafix.utils.RecyclerItemClickListener;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;


public class MainFragment extends BaseFragment implements MainFragmentView{

    private static final String ARG_PARAM_CONTACTS = "contacts";

    @Inject
    ContactController contactController;

    @BindView(R.id.rvContacts)
    RecyclerView rvContacts;
    @BindView(R.id.llWithoutElement)
    LinearLayout llWithoutElement;
    @BindView(R.id.ivStatus)
    ImageView ivStatus;
    @BindView(R.id.tvDetail)
    TextView tvDetail;

    private MainFragmentPresenter mainFragmentPresenter;
    private ArrayList<Contact> contacts;
    private StaggeredGridLayoutManager mLayoutManager;
    private ContactAdapter mAdapter;

    public MainFragment() {
        // Required empty public constructor
    }


    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
        setHaveToolbar(true);
        if(savedInstanceState != null){
            contacts = savedInstanceState.getParcelableArrayList(ARG_PARAM_CONTACTS);
        }
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_main;
    }

    @Override
    public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        setTitle(R.string.main_contacts);
        setupImageToolbar(R.drawable.ic_launch, false);
        if((savedInstanceState == null) || (contacts == null)){
           loadContacts();
        }else{
            isContactEmpty();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(ARG_PARAM_CONTACTS, contacts);
    }

    private void initialize() {
        getApplicationComponent().inject(this);
        mainFragmentPresenter = new MainFragmentPresenter(this, contactController);
    }

    private void loadContacts() {
        showProgressDialog();
        mainFragmentPresenter.getContacts();
    }

    public void isContactEmpty() {
        if(contacts.isEmpty()){
            llWithoutElement.setVisibility(View.VISIBLE);
            rvContacts.setVisibility(View.GONE);
        }else{
            rvContacts.setVisibility(View.VISIBLE);
            llWithoutElement.setVisibility(View.GONE);
        }
    }

    @Override
    public void setContactsAdapter(ArrayList<Contact> contacts) {
        dismissDialog();
        this.contacts = contacts;
        isContactEmpty();
        if (!contacts.isEmpty()){
            mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
            rvContacts.setLayoutManager(mLayoutManager);
            mAdapter = new ContactAdapter(getContext(), Glide.with(MainFragment.this), contacts);

            rvContacts.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), (view, position) -> {
                pushFragment(DetailContactFragment.newInstance(contacts.get(position).getUser_id()));
            }));
            rvContacts.setAdapter(mAdapter);
        }
    }

    @Override
    public void onFailure() {
        dismissDialog();
        llWithoutElement.setVisibility(View.VISIBLE);
        ivStatus.setImageResource(R.drawable.ic_error_connection);
        tvDetail.setText(R.string.main_error_contact);
    }

    @OnClick(R.id.ivStatus)
    public void onClick(View v){
        switch (v.getId()){
            case R.id.ivStatus:
                loadContacts();
                break;
        }
    }

}

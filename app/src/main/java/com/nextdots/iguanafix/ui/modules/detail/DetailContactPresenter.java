package com.nextdots.iguanafix.ui.modules.detail;


import com.nextdots.iguanafix.api.controllers.ContactController;
import com.nextdots.iguanafix.data.models.Contact;
import com.nextdots.iguanafix.ui.modules.main.MainFragmentView;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DetailContactPresenter {
    private final DetailFragmentView detailFragmentView;
    private ContactController contactController;

    public DetailContactPresenter(DetailFragmentView detailFragmentView, ContactController contactController) {
        this.detailFragmentView = detailFragmentView;
        this.contactController = contactController;
    }

    public void getContact(String contact_id) {
        contactController.getDetail(contact_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onFailure);
    }

    public void onSuccess(Contact contacts){
        detailFragmentView.setDetailContact(contacts);
    }

    public void onFailure(Throwable throwable){
        detailFragmentView.onFailure();
    }
}

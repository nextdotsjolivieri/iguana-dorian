package com.nextdots.iguanafix.ui.base;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.ArrayRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.nextdots.iguanafix.R;
import com.nextdots.iguanafix.data.models.PermissionResult;
import com.nextdots.iguanafix.di.components.ApplicationComponent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;


public abstract class BaseFragment extends Fragment {
    private static final String TAG = BaseFragment.class.getSimpleName();
    public static final int TRANSACTION_WITHOUT_ANIMATION = 0;
    private static final String KEY_ERROR = "errror";

    private BaseActivity activity;
    private Map<String, String> permissionDescriptions;
    public boolean haveToolbar;
    private Toolbar mToolbar;
    private int resToolbar = 0;
    private boolean errorOverrideCreateView;
    private TextView tvTitle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = (BaseActivity) getActivity();
        permissionDescriptions = new HashMap<>();
        errorOverrideCreateView = savedInstanceState == null || savedInstanceState.getBoolean(KEY_ERROR);

        permissionDescriptions.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string
                .permission_storage_desc));
        permissionDescriptions.put(Manifest.permission.CAMERA, getString(R.string
                .permission_camera_desc));
    }

    abstract public int getLayoutView();

    abstract public void onViewReady(LayoutInflater inflater, @Nullable ViewGroup container,
                                     @Nullable Bundle savedInstanceState, View view);

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        activity = (BaseActivity) getActivity();
        permissionDescriptions = new HashMap<>();
        permissionDescriptions.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string
                .permission_storage_desc));
        permissionDescriptions.put(Manifest.permission.CAMERA, getString(R.string
                .permission_camera_desc));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(getLayoutView(), container, false);
        ButterKnife.bind(this, view);
        if (haveToolbar) {
            onCreateToolbar(view, resToolbar, null);
        }
        onViewReady(inflater, container, savedInstanceState, view);
        errorOverrideCreateView = false;
        return view;
    }


    public void onCreateToolbar(View view, int resToolbar, Toolbar toolbar) {
        if (toolbar == null) {
            try {
                if (resToolbar == 0) {
                    resToolbar = R.id.toolbar;
                }
                mToolbar = (Toolbar) view.findViewById(resToolbar);
                tvTitle = (TextView) view.findViewById(R.id.tvTitle);
                setToolBar(mToolbar);
                activity.hideToolbar();
                haveToolbar = true;
                setEnableBackToolbar(true);
            } catch (Exception e) {
                Log.e(TAG + " ", "");
                haveToolbar = false;
            }
        } else {
            mToolbar = toolbar;
            haveToolbar = true;
        }
    }

    public void setupImageToolbar(@NonNull @DrawableRes int resImage, boolean enable) {
        activity.setupImageToolbar(resImage, enable);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (errorOverrideCreateView) {
            throw new RuntimeException("You should call the super for onCreateview");
        }

        if (haveToolbar) {
            activity.hideToolbar();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (haveToolbar && !haveToolbarLastFragment()) {
            activity.showToolbar();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_ERROR, errorOverrideCreateView);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

    public void setHaveToolbar(boolean haveToolbar) {
        this.haveToolbar = haveToolbar;
    }

    protected void showProgressDialog() {
        activity.showProgressDialog();
    }

    protected void pushFragment(Fragment fragment) {
        activity.pushFragment(fragment);
    }

    protected void pushFragment(Fragment fragment, int container, boolean addBackStack) {
        activity.pushFragment(fragment, container, addBackStack);
    }

    protected void dismissDialog() {
        if (activity != null) {
            activity.dismissProgressDialog();
        }
    }

    protected void setTitle(@NonNull @StringRes int resId) {
        setTitle(getContext().getString(resId));
    }

    protected void setTitle(String title) {
        if (haveToolbar) {
            mToolbar.setTitle("");
            mToolbar.setSubtitle("");
            tvTitle.setText(title);
        } else {
            activity.setTitle(title);
        }
    }

    protected void requestPermission(final String permission) {
        // Should we show an explanation?
        if (shouldShowRequestPermissionRationale(permission)) {
            String text = permissionDescriptions.get(permission);

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    requestPermissions(new String[]{permission}, 0);
                    dismissDialog();
                }

            };

            showMessageOKCancel(text, listener);
        } else {
            requestPermissions(new String[]{permission}, 0);
        }
    }

    public boolean onBackPressed() {
        return false;
    }

    public boolean onBackToolbar(){
        return false;
    }


    protected void goBack() {
        activity.goBack();
    }

    public void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog
                .Builder(getActivity(), R.style.MyDialogTheme)
                .setTitle(getString(R.string.app_name))
                .setMessage(message)
                .setPositiveButton(R.string.ok, okListener)
                .create()
                .show();
    }

    public void setToolBar(Toolbar toolBar) {
        BaseActivity baseActivity = (BaseActivity) getActivity();
        if (baseActivity != null) {
            mToolbar = toolBar;
            haveToolbar = true;
            ((BaseActivity) getActivity()).setSupportActionBar(toolBar);
        }
    }

    public void setEnableBackToolbar(boolean enable) {
        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
            activity.getSupportActionBar().setDisplayShowHomeEnabled(enable);
        }
    }

    public void startActivity(Intent intent) {
        activity.startActivity(intent);
    }

    public String getLastTagFragment() {
        return activity.getLastTagFragment();
    }

    public boolean haveToolbarLastFragment() {
        String tag = getLastTagFragment();
        BaseFragment baseFragment = activity.findFragmentByTag(tag);
        return baseFragment != null && (!tag.isEmpty() && baseFragment.haveToolbar);
    }

    //----------------------------------------------------------------------------------------------
    // Permission Methods
    //----------------------------------------------------------------------------------------------

    public boolean hasPermissions(String... permissions) {
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public boolean hasPermissionsOrRequest(int requestCode, String... permissions) {
        List<String> denied = new ArrayList<>();

        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    permission) != PackageManager.PERMISSION_GRANTED) {
                denied.add(permission);
            }
        }

        if (denied.size() > 0) {
            this.requestPermissions(denied.toArray(new String[denied.size()]), requestCode);
            return false;
        }
        return true;
    }

    private boolean hasPermissionsOrRequest(Activity activity, int requestCode, String... permissions) {
        if (BaseActivity.class.isAssignableFrom(activity.getClass())) {
            return ((BaseActivity) activity).hasPermissionsOrRequest(requestCode, permissions);
        }
        throw new IllegalArgumentException("The Activity " + activity.getClass().getSimpleName()
                + " needs to be extended from BaseActivity ");
    }

    public ApplicationComponent getApplicationComponent() {
        return this.activity.getApplicationComponent();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        onRequestPermissionsResult(PermissionResult.getResult(getActivity(),
                requestCode, permissions, grantResults));
    }

    public void onRequestPermissionsResult(PermissionResult result) {

    }
}

package com.nextdots.iguanafix.ui.modules.detail;


import com.nextdots.iguanafix.data.models.Contact;

import java.util.List;

public interface DetailFragmentView {
    void setDetailContact(Contact contact);
    void onFailure();
}

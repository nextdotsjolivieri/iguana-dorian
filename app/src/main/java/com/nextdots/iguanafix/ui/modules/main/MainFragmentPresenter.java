package com.nextdots.iguanafix.ui.modules.main;


import com.nextdots.iguanafix.api.controllers.ContactController;
import com.nextdots.iguanafix.data.models.Contact;

import java.util.ArrayList;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class MainFragmentPresenter {

    private final MainFragmentView mainFragmentView;
    private ContactController contactController;

    public MainFragmentPresenter(MainFragmentView mainFragmentView, ContactController contactController) {
        this.mainFragmentView = mainFragmentView;
        this.contactController = contactController;
    }


    public void getContacts(){
        contactController.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onFailure);

    }

    public void onSuccess(ArrayList<Contact> contacts){
        mainFragmentView.setContactsAdapter(contacts);
    }

    public void onFailure(Throwable throwable){
        mainFragmentView.onFailure();
    }

}

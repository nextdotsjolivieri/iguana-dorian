package com.nextdots.iguanafix.ui.modules.main;


import com.nextdots.iguanafix.data.models.Contact;

import java.util.ArrayList;


public interface MainFragmentView {
    void setContactsAdapter(ArrayList<Contact> contacts);
    void onFailure();
}

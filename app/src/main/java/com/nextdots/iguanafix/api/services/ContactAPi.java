package com.nextdots.iguanafix.api.services;

import com.nextdots.iguanafix.data.models.Contact;

import java.util.ArrayList;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;



public interface ContactAPi {
    @GET("contacts")
    Observable<ArrayList<Contact>> getAll();

    @GET("/contacts/{contact_id}")
    Observable<Contact> getDetail(@Path("contact_id") String contact_id);
}

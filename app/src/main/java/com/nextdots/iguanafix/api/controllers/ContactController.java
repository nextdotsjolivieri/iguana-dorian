package com.nextdots.iguanafix.api.controllers;

import com.nextdots.iguanafix.api.services.ContactAPi;
import com.nextdots.iguanafix.data.models.Contact;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ContactController {
    private final ContactAPi contactApi;

    public ContactController(ContactAPi cityAPi) {
        this.contactApi = cityAPi;
    }

    public Observable<ArrayList<Contact>> getAll() {
        return contactApi.getAll().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Contact> getDetail(String contact_id) {
        return contactApi.getDetail(contact_id).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}

package com.nextdots.iguanafix.utils;


import android.support.annotation.DrawableRes;

import com.nextdots.iguanafix.R;

import java.util.Random;

public class GlideHelper {

    public static @DrawableRes int getRamdonCircularColor(){
        Random random = new Random();
        switch (random.nextInt(4)){
            case 0:
                return R.drawable.circle_blue;
            case 1:
                return R.drawable.circle_green;
            case 2:
                return R.drawable.circle_pink;
            default:
                return R.drawable.circle_gray;
        }
    }
}

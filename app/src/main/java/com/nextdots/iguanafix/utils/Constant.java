package com.nextdots.iguanafix.utils;



public interface Constant {

    String URL_SERVER = "https://private-d0cc1-iguanafixtest.apiary-mock.com/";

    String TYPE_HOME = "Home";
    String TYPE_CELLPHONE = "Cellphone";
    String TYPE_OFFICE = "Office";
}
